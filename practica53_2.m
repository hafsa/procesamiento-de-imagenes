%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Práctica 5.3_2
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Read "pout.tif" y "tire.tif" en img1 e img2
img1 = imread("pout.tif");
img2 = imread("tire.tif");

%% Muestre las imagenes y los histogramas en 2 figuras 1x3 usando 1/3 del espacio para la imagen y 2/3 para el histograma
figure;
subplot(1,3,1);
imshow(img1);
subplot(1,3,[2,3]);
imhist(img1);
figure;
subplot(1,3,1);
imshow(img2);
subplot(1,3,[2,3]);
imhist(img2);

%% Ecualice Linealmente imgX y represente su histogramas en 2 figuras diferentes con las mismas proporciones que en el caso anterior
img1_LEq = uint8(double(img1- min(img1(:))) ./ double(max(img1(:)) - min(img1(:))) .* 256);
figure;
subplot(1,3,1);
imshow(img1_LEq);
subplot(1,3,[2,3]);
imhist(img1_LEq);

img2_LEq = uint8(double(img2- min(img2(:))) ./ double(max(img2(:)) - min(img2(:))) .* 256);
figure;
subplot(1,3,1);
imshow(img2_LEq);
subplot(1,3,[2,3]);
imhist(img2_LEq);

%% Calcule la frecuencia y la localización de los bines del histograma de 64 de cada imagen. Use "imhist"
[img1_freq,img1_binLocations] = imhist(img1, 64);
[img2_freq,img2_binLocations] = imhist(img2, 64);

%% Ecualice las imagenes usando el histograma con "histeq" con 64 bins. Calcule la transformación T. Represente el resultado y su histograma
[img1_UHEq,T_img1] = histeq(img1, 64);
[img2_UHEq,T_img2] = histeq(img2, 64);
figure;
subplot(1,2,1);imshow(img1_UHEq);
subplot(1,2,2);imhist(img1_UHEq);

figure;
subplot(1,2,1);
imshow(img1_UHEq);
subplot(1,2,2);
imhist(img1_UHEq);

%% Replicamos con código "histeq" por nuestra cuenta para la img1
n_bins_img1 = 64;
nImax_img1 = 256;

img1_freq_cum = cumsum(img1_freq);

nPx_img1 = numel(img1); %n pixels

% probabilidad de cada nivel de intensidad
probf_img1 = img1_freq / nPx_img1;

% probabilidad acumulativa
probc_img1 = img1_freq_cum / nPx_img1;



% Muestre img1_freq, img1_freq_cum, probf_img1 y probc_img1 en una imagen
% 2x2
figure;
subplot(1,4,1);imshow(img1_freq);
subplot(1,4,2);imshow(img1_freq_cum);
subplot(1,4,3);imshow(probf_img1);
subplot(1,4,4);imshow(probc_img1);
grid on;

[N_img1,edges_img1] = histcounts(img1, n_bins_img1, 'Normalization','cdf');
T2_img1 = mkpp(edges_img1,N_img1.*nImax_img1);
img1_UHEq2 = uint8(ppval(T2_img1,double(img1+1)));

%% Muestre img1, img1_UHEq y img1_UHEq2
figure;
subplot(1,3,1);imshow(img1);
subplot(1,3,2);imshow(img1_UHEq);
subplot(1,3,3);imshow(img1_UHEq2);

%% Repita el apartado anterior para la img2
n_bins_img2 = 64;
nImax_img2 = 256;

img2_freq_cum = cumsum(img2_freq);

nPx_img2 = numel(img2); %n pixels

% probabilidad de cada nivel de intensidad
probf_img2 = img2_freq / nPx_img2;

% probabilidad acumulativa
probc_img2 = img2_freq_cum / nPx_img2;


% Muestre img2_freq, img2_freq_cum, probf_img2 y probc_img2 en una imagen
% 2x2
figure;
subplot(1,4,1);imshow(img2_freq);
subplot(1,4,2);imshow(img2_freq_cum);
subplot(1,4,3);imshow(probf_img2);
subplot(1,4,4);imshow(probc_img2);
grid on;


[N_img2,edges_img2] = histcounts(img2, n_bins_img2, 'Normalization','cdf');
T2_img2 = mkpp(edges_img2,N_img2.*nImax_img2);
img2_UHEq2 = uint8(ppval(T2_img2,(double(img2+1))));

%% Muestre img2, img2_UHEq y img2_UHEq2
figure;
subplot(1,3,1);imshow(img2);
subplot(1,3,2);imshow(img2_UHEq);
subplot(1,3,3);imshow(img2_UHEq2);


%% Crea una imagen imgX_b restandole el minimo valor de su inrensidad.
img1_b = img1 - min(img1(:));
img2_b = img2 - min(img2(:));

%% Representelas junto con sus histogramas en 2 figuras
figure;
subplot(1,2,1);
imshow(img1_b);
title('Imagen 1 - Valor Mínimo');
subplot(1,2,2);
imhist(img1_b);

figure;
subplot(1,2,1);
imshow(img2_b);
title('Imagen 2 - Valor Mínimo');
subplot(1,2,2);
imhist(img2_b);


%% Ecualice la img1_b con la transformación logaritmica para ocupar el máximo rango dinámico
MaxImg1 = double(max(img1_b(:)));
K1 = 255 / log(1 + MaxImg1);
img1_log = uint8(K1 * log(double(img1_b) + 1));

%% Represente en una figura 1x3: img1, img1_b y img1_log
figure;
subplot(1,3,1);
imshow(img1);
title('Imagen Original');
subplot(1,3,2);
imshow(img1_b);
title('Imagen con Min. Intensidad');
subplot(1,3,3);
imshow(img1_log);
title('Imagen con Transformación Logarítmica');

%% Repita el apartado anterior para la img2. Elimine todas las intensidades superiores a 150
%MaxImg2 = double(max(img2_b(:)));
MaxImg2 = 150;
K2 = 255 / log(1 + MaxImg2);
img2_log = uint8(K2 * log(double(img2)+1));

figure;
subplot(1,3,1);
imshow(img2);
title('Imagen Original');
subplot(1,3,2);
imshow(img2_b);
title('Imagen con Min. Intensidad');
subplot(1,3,3);
imshow(img2_log);
title('Imagen con Transformación Logarítmica y Max. 150');