%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Prac 5.4
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Carga la imagen 'Board_Recorte.tif' y muestra la imagen
img = imread("Board_Recorte.tif");
figure;
imshow(img);
%% Separe los canales de color RGB y muestrelos en una imagen 1x3
imgR = img(:, :, 1);
imgG = img(:, :, 2);
imgB = img(:, :, 3);
figure;
subplot(1,3,1); 
imshow(imgR);
subplot(1,3,2); 
imshow(imgG);
subplot(1,3,3);
imshow(imgB);
%% Convierta la imagen RGB en HSI y represente cada canal en una imagen 1x3
[imgH,imgS,imgI] =  rgb2hsi(img);
figure;
subplot(1,3,1);
imshow(imgH);
subplot(1,3,2);
imshow(imgS);
subplot(1,3,3);
imshow(imgI);
%% Negativo del canal de saturación en "imgSNeg"
% Muestre tanto el negativo como su histograma en una imagen
imgSNeg= 1 - imgS;
figure;
subplot(1,2,1);
imshow(imgSNeg);
subplot(1,2,2);
imhist(imgSNeg);
%% Unmbralice "imgSNeg" con un nivel obtenido por el método de Otsu (graythresh). Almacene el resultado en "imgB"
level = graythresh(imgSNeg);
imgB = imbinarize(imgSNeg, level);
% Muestre la imagen umbralizada
figure; 
imshow(imgB);
title('Imagen Umbralizada por el Método de Otsu');
%% Filtre la imagen imgB con un filtro de mediana 5x5. Muestre el resultado
B = medfilt2(imgB, [5, 5]);
figure;
imshow(B);
%% Operacion Morfologicas: La teoría de esta parte de la práctica no se verá hasta la siguiente sesión.
%% No obstante, es necesario utilizar algunas operaciones para continuar con la práctica de segmentación
EE_cuadrado = strel("square",35); % define un elemento estructurante  cuadrado de tamaño 35 con la función "strel"
B_Open = imopen(B,EE_cuadrado); % Aplique una apertura "imopen" a la imagen filtrada anterior "B" con el elemento estructurante anterior
% Represente la imagen resultante
figure
imshow(B_Open);
title('Open');
%% Realice las operaciones de erosión, dilatación y cierre y represente el resultado
B_erode =imerode(B,EE_cuadrado);
figure
imshow(B_erode);
title('Erosion');
B_dilate = imdilate(B,EE_cuadrado);
figure
imshow(B_dilate);
title('Dilate');
B_Close = imclose(B,EE_cuadrado);
figure
imshow(B_Close);
title('Close');
%% Ahora, segmente la imagen binaria "B_Open".
[IM_Seg, Num_objetos]=  bwlabel(B_Open);
% Represente la imagen de las etiquetas con la función "label2rgb"
RGB_Segment = label2rgb(IM_Seg);
figure
imshow(RGB_Segment);
title("Imagen B_Open segmentada");
%% Utilice la función "regionprops" para poder extraer propiedades de los objetos encontrados
Props = regionprops(IM_Seg, 'Eccentricity'); %más excéntrico: más largo
imtool(IM_Seg, []);
V_Excent = [];
V_Excent = [V_Excent Props(1).Eccentricity];
V_Excent = [V_Excent Props(2).Eccentricity];
V_Excent = [V_Excent Props(3).Eccentricity];
V_Excent = [V_Excent Props(4).Eccentricity];
V_Excent = [V_Excent Props(5).Eccentricity];
V_Excent = [V_Excent Props(6).Eccentricity];
V_Excent = [V_Excent Props(7).Eccentricity];
% Represente la excentricidad con "stem"
figure,
stem(V_Excent)