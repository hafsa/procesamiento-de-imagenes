clc; 
close all;

%% Leer imágenes
img1 = imread('coins.png');
img2 = imread('peppers.png');

%% Resolución Espacial
resE_img1 = size(img1);
resE_img2 = size(img2);

%% Tamaño en bytes
tamB_img1 = numel(img1);
tamB_img2 = numel(img2);

%% Use rgb2gray para pasar la imagen 2 a escala de grises
img2_g = rgb2gray(img2);

%% Obtenga los canales R, G y B por separado de la imagen img2
R_img2 = img2(:, :, 1);
G_img2 = img2(:, :, 2);
B_img2 = img2(:, :, 3);

%% Ecualizar linealmente la imagen img1 e img2_g: imY = (imX-minimo(img1)) ./ (maximo(imX)-minimo(imX))
img1_n = uint8(double(img1 - min(img1(:))) ./ double(max(img1(:)) - min(img1(:))) .* 256);
img2_g_n = uint8(double(img2_g - min(img2_g(:))) ./ double(max(img2_g(:)) - min(img2_g(:))) .* 256);


%% Representa img1 y su correspondiente imagen ecualizada en 1x2. pon titulos
figure; 
subplot(1, 2, 1);
imshow(img1);
title('Imagen 1 original');
subplot(1, 2, 2);
imshow(img1_n);
title('Imagen 1 ecualizada');

%% Representa img2 en escala de grises y su correspondiente imagen ecualizada en 1x2. Poga títulos por subplot
figure;
subplot(1, 2, 1);
imshow(img2_g);
title('Imagen 2 en escala de grises');
subplot(1, 2, 2);
imshow(img2_g_n);
title('Imagen 2 ecualizada en escala de grises');

%% Representa la imagen img2 a color
figure;
imshow(img2);
title('Imagen 2 a Color');

%% Representa en una figura 1x3 cada canal de la imagen img2
figure;
subplot(1, 3, 1);
imshow(R_img2);
title('Canal Rojo (R)');
subplot(1, 3, 2);
imshow(G_img2);
title('Canal Verde (G)');
subplot(1, 3, 3);
imshow(B_img2);
title('Canal Azul (B)');

%% Reduzca muestre la imagen un factor de 10 con los métodos: el mas cercano, bilineal, bicubica
factor = 10;
img1_res1 = imresize(img1, 1/factor, 'nearest');
img1_res2 = imresize(img1, 1/factor, 'bilinear');
img1_res3 = imresize(img1, 1/factor, 'bicubic');

%% Aumenta las imagenes reducidas un factor de 10 con los métodos: el mas cercano, bilineal, bicubica

img1_aug1 = imresize(img1_res1, factor, 'nearest');
img1_aug2 = imresize(img1_res2, factor, 'bilinear');
img1_aug3 = imresize(img1_res3, factor, 'bicubic');


%% Muestre las imagenes recuperadas en una figura 1x3
figure;
subplot(1, 3, 1);
imshow(img1_res1)
title('Reducción - vecino más cercano');
subplot(1, 3, 2);
imshow(img1_res2);
title('Reducción - bilineal');
subplot(1, 3, 3);
imshow(img1_res3)
title('Reducción - bicúbica');

figure;
subplot(1, 3, 1);
imshow(img1_aug1)
title('Aumento - vecino más cercano');
subplot(1, 3, 2);
imshow(img1_aug2);
title('Aumento - bilineal');
subplot(1, 3, 3);
imshow(img1_aug3)
title('Aumento - bicúbica');
