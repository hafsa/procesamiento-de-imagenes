%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% prac 5.5
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clc; clear all; close all
% Carga la imagen 'text.png'
img = imread('text.png');
% Muestra la image
figure; imshow(img), title('Original');
% Genera 3 elementos estructurantes tipo linea de de tamaño 11 a 90º,0º,y45
% y guardalos en se1, se2, se3 respectivamente
se1 = strel('line', 11, 90);
se2 = strel('line', 11, 0);
se3 = strel('line', 11, 45);
% Dilate la imagen original con cada uno de los elementos estructurantes
imgD1 = imdilate(img, se1);
imgD2 = imdilate(img, se2);
imgD3 = imdilate(img, se3);
% Muestre las tres imagenes en una figura 1x3
figure; 
subplot(1,3,1); imshow(imgD1), title('Dilated V')
subplot(1,3,2); imshow(imgD2), title('Dilated H')
subplot(1,3,3); imshow(imgD3), title('Dilated 45')

% Erosione la imagen original con cada uno de los elementos estructurantes
% anteriores
imgE1 = imerode(img, se1);
imgE2 = imerode(img, se2);
imgE3 = imerode(img, se3);

% Muestre las tres imagenes de la erosión en una figura 1x3
figure; 
subplot(1,3,1); imshow(imgE1), title('Eroded V')
subplot(1,3,2); imshow(imgE2), title('Eroded H')
subplot(1,3,3); imshow(imgE3), title('Eroded 45')

% Realice una apertura de la la imagen original con cada uno de los
% elementos estructurantes anteriores
imgO1 = imdilate(imgE1, se1);
imgO2 = imdilate(imgE2, se2);
imgO3 = imdilate(imgE3, se3);

% Muestre las tres imagenes de la erosión en una figura 1x3
figure; 
subplot(1,3,1); imshow(imgO1), title('Opened V')
subplot(1,3,2); imshow(imgO2), title('Opened H')
subplot(1,3,3); imshow(imgO3), title('Opened 45')

% Realice una apertura de la la imagen original con cada uno de los
% elementos estructurantes anteriores
imgC1 = imerode(imgD1, se1);
imgC2 = imerode(imgD2, se2);
imgC3 = imerode(imgD3, se3);

% Muestre las tres imagenes de la erosión en una figura 1x3
figure; 
subplot(1,3,1); imshow(imgC1), title('Closed V')
subplot(1,3,2); imshow(imgC2), title('Closed H')
subplot(1,3,3); imshow(imgC3), title('Closed 45')

% Carga la imagen 'coins.png' en img2
img2 = imread('coins.png');
% Añada ruido Salt and Peper con densidad 0.1
img2n = imnoise(img2, 'Salt & Pepper', 0.1);
% Muestre la imagen original y la contaminada con ruido en una figura 1x2
figure;subplot(1,2,1);imshow(img2);subplot(1,2,2);imshow(img2n);
% Binarice la señal ruidosa con un threshold de 0.4
img2B = imbinarize(img2n, 0.4);
% Genere un elemento estructurante con forma de disco de tamaño 1
se_o = strel('disk', 1);
% Realice una apertura de la imagen binarizada
img2Bop = imdilate(imerode(img2B, se_o), se_o);
% Genere un elemento estructurante con forma de disco de tamaño 2
se_c = strel('disk', 2);
% Realice un cierre de la imagen img2Bop
img2Bopcl = imerode(imdilate(img2B, se_o), se_o);
% Muestre la imagen img2B y la filtrada con filtros morfológicos img2Bopcl
figure;subplot(1,2,1);imshow(img2B);subplot(1,2,2);imshow(img2Bopcl);
% Genere una mascara de un filtro de borde isométrico
mks2 = [-1 -1 -1; -1 8 -1; -1 -1 -1]; % DIAPOSITIVA 33 5.3
% Filtre la imagen con dicha máscara
imgBo = imfilter(img2Bopcl, mks2);
% Represente la imagen final
figure; imshow(imgBo);