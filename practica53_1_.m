%%% Practica 5.3 %%%
clc; 
clear all; 
close all;
rng(10);
%% Lea las imagenes "vista_ciudad.TIF", "coins.png" y "peppers.png"
% La 1º imagen se debe cargar en img1, la 2º en img2 y la 3º en img3
img1 = imread('vista_ciudad.TIF');
figure, imshow(img1);

img2 = imread('coins.png');
figure, imshow(img2);

img3 = imread('peppers.png');
figure, imshow(img3);

%figure, imshow(img1,[]); %sube el contraste de la imagen

%% Tamaño Lea las imagenes "vista_ciudad.TIF", "coins.png" y "peppers.png"
s_img1 = size(img1);
s_img2 = size(img2);
s_img3 = size(img3);

%% Ecualice linealmente las imagenes 1 y 2 en img1_s y img2_s
img1_s = uint8(double(img1- min(img1(:))) ./ double(max(img1(:)) - min(img1(:))) .* 256);
img2_s = uint8(double(img2- min(img2(:))) ./ double(max(img2(:)) - min(img2(:))) .* 256);

%% Visualice en 2 Figuras cada una de 1x3: imgX, imshow(img1,[]); imgX_s
figure(1); 
subplot(1,3,1); 
imshow(img1);
subplot(1,3,2);
imshow(img1, []);
subplot(1,3,3);
imshow(img1_s);

figure(2);
subplot(1,3,1); 
imshow(img2);
subplot(1,3,2);
imshow(img2, []);
subplot(1,3,3);
imshow(img2_s);

%% Convierta la imagen 3 a escala de grises en img3_g, ecualice la imagen y cree una figura como la anterior
img3_g = rgb2gray(img3);
img3_g_s = uint8(double(img3_g- min(img3_g(:))) ./ double(max(img3_g(:)) - min(img3_g(:))) .* 256);
figure(3); 
subplot(1,3,1); 
imshow(img3_g);
subplot(1,3,2);
imshow(img3_g, []);
subplot(1,3,3);
imshow(img3_g_s);

%% Obtenga los canales R, G y B por separado de la imagen img3
R_img3 = img3(:, :, 1);
G_img3 = img3(:, :, 2);
B_img3 = img3(:, :, 3);

%% Visualice los canales R, G y B por separado de la imagen img3
figure;
subplot(1,3,1); 
imshow(R_img3); 
title('R');
subplot(1,3,2); 
imshow(G_img3); 
title('G');
subplot(1,3,3); 
imshow(B_img3);
title('B');

%% Cree una imagen como img3 por con canal R con el negativo del canal R
img3_n = img3;
img3_n(:,:,1)  = 256 - img3_n(:,:,1);
%% Visualice la imagen img3 y la nueva
figure;
subplot(1,2,1);
imshow(img3);
title('original')
subplot(1,2,2);
imshow(img3_n);
title('negativo Rojo')


%% Generar muestra de tamaño 10^5 de una Gaussiana de media 1 y varianza 2 y represente el histograma
nVec = 10^5;
muestra_gauss = 1 + sqrt(2) * randn(1, nVec);
figure;
hist(muestra_gauss, 50);
title('Histograma de la Distribución Gaussiana de media 1 y varianza 2');
%% Histograma de img2: Use la función imhist
figure; 
imhist(img2, 50);
title('Histograma de la imagen 2');
%% Umbralice img2 fijando un umbral a 0.5
BW1 = img2bw(img2, 0.5);
% Use la funcion "imshowpair" para mostrar img2 y la imagen umbralizada
figure; 
imshowpair(img2, BW1, 'montage');
title('Imagen Original y Binarizada con Umbral 0.5');

%% Use la funcion "graythresh" encontrar un mejor umbral que manualmente
% Umbralice con este nuevo umbral y de nuevo muestre el resultado de img2 y
% de su imagen umbralizada
level = graythresh(img2);
BW2 = img2bw(img2, level);
figure;
imshowpair(img2, BW2, 'montage');
title('Imagen Original y Binarizada con Umbral Óptimo');

%% Genere una imagen img4 cuadrada de 256 pixeles con un nivel de intensidad 128
img4 = 128 * ones(256, 'uint8');
%% Genere 3 nuevas imagenes con ruido gaussiano, salt and pepper y speckle con potencia media de ruido 0.02
img4_g = imnoise(img4, 'gaussian',0, 0.02);
img4_sp = imnoise(img4, 'salt & pepper', 0.02);
img4_s = imnoise(img4, 'speckle', 0.02);

%% Represente las tres imagenes en una figura 1x3
figure;
subplot(1,3,1); imshow(img4_g); title('gaussian');
subplot(1,3,2); imshow(img4_gp);title('salt & pepper');
subplot(1,3,3); imshow(img4_s);title('speckle');
%% Represente sus tres histogramas en una figura 1x3
figure;
subplot(1,3,1); imhist(img4_g, 50); title('gaussian');
subplot(1,3,2); imhist(img4_gp, 50);title('salt & pepper');
subplot(1,3,3); imhist(img4_s, 50) ;title('speckle');

%% Filtrado Lineal
% Filtre img4_g con una mascara de suavizado 5x5 con la estrategia zero
% padding y mirror padding en guarda el resultado en img_g_fzer y img_g_fsym
msk = ones(5)/25;
img_g_fzer = imfilter(img4_g, msk);
img_g_fsym = imfilter(img4_g, msk, 'symmetric');

% Represente en una figura 1x3 img4,img4_g y img_g_fzer
figure;
title('Gauss');
subplot(1,3,1); imshow(img4);
subplot(1,3,2); imshow(img4_g);
subplot(1,3,3); imshow(img_g_fzer);

% Represente en una figura 2x2 img_g_fzer, img_g_fsym y sus histogramas
figure;
title('Gauss');
subplot(2,2,1); imshow(img_g_fzer);
subplot(2,2,2); imshow(img_g_fsym);
subplot(2,2,3); imhist(img_g_fzer, 50);
subplot(2,2,4); imhist(img_g_fsym, 50);

% Filtre img4_sp con una mascara de suavizado 5x5 con la estrategia zero
% padding y mirror padding en guarda el resultado en img_sp_fzer y img_sp_fsym
msk = ones(5)/25;
img_sp_fzer = imfilter(img4_sp, msk);
img_sp_fsym = imfilter(img4_sp, msk, 'symmetric');

% Represente en una figura 1x3 img4,img4_sp y img_sp_fzer
figure;
title('Salt and Pepper');
subplot(1,3,1); imshow(img4);
subplot(1,3,2); imshow(img4_sp);
subplot(1,3,3); imshow(img_sp_fzer);

% Represente en una figura 2x2 img_sp_fzer, img_sp_fsym y sus histogramas
figure;
title('Salt and Pepper');
subplot(2,2,1); imshow(img_sp_fzer);
subplot(2,2,2); imshow(img_sp_fsym);
subplot(2,2,3); imhist(img_sp_fzer, 50);
subplot(2,2,4); imhist(img_sp_fsym, 50);

% Filtre img4_s con una mascara de suavizado 5x5 con la estrategia zero
% padding y mirror padding en guarda el resultado en img_s_fzer y img_s_fsym
msk = ones(5)/25;
img_s_fzer = imfilter(img4_s, msk);
img_s_fsym = imfilter(img4_s, msk,'symmetric');

% Represente en una figura 1x3 img4,img4_s y img_s_fzer
figure;
title('speckle');
subplot(1,3,1); imshow(img4);
subplot(1,3,2); imshow(img4_s);
subplot(1,3,3); imshow(img_s_fzer);

% Represente en una figura 2x2 img_s_fzer, img_s_fsym y sus histogramas
figure;
title('speckle');
subplot(2,2,1); imshow(img_s_fzer);
subplot(2,2,2); imshow(img_s_fsym);
subplot(2,2,3); imhist(img_s_fzer, 50);
subplot(2,2,4); imhist(img_s_fsym, 50);
%% Filtrado Mediana
% Filtre img4_g con un filtro de mediana 5x5 con la estrategia zero
% padding y mirror padding en guarda el resultado en img_fmedzer y img_fmedsym

img_g_fmedzer = medfilt2(img4_g, [5, 5], 'zeros');
img_g_fmedsym = medfilt2(img4_g, [5, 5], 'symmetric');

% Represente en una figura 1x3 img4,img4_g y img_fmedzer
figure;
title('Gauss');
subplot(1,3,1); imshow(img4);
subplot(1,3,2); imshow(img4_g);
subplot(1,3,3); imshow(img_g_fmedzer);

% Represente en una figura 2x2 img_g_fmedzer, img_g_fmedsym y sus histogramas
figure;
title('Gauss');
subplot(2,2,1); imshow(img_g_fmedzer);
subplot(2,2,2); imshow(img_g_fmedsym);
subplot(2,2,3); imhist(img_g_fmedzer, 50);
subplot(2,2,4); imhist(img_g_fmedsym, 50);

% Filtre img4_sp con un filtro de mediana 5x5 con la estrategia zero
% padding y mirror padding en guarda el resultado en img_sp_fmedzer y img_sp_fmedsym
img_sp_fmedzer = medfilt2(img4_sp, [5, 5], 'zeros');
img_sp_fmedsym = medfilt2(img4_sp, [5, 5], 'symmetric');

% Represente en una figura 1x3 img4, img4_sp y img_sp_fmedzer
figure; 
title('Salt and Pepper');
subplot(1,3,1); imshow(img4);
subplot(1,3,2); imshow(img4_sp);
subplot(1,3,3); imshow(img_sp_fmedzer);

% Represente en una figura 2x2 img_sp_fmedzer, img_sp_fmedsym y sus histogramas
figure;
title('Salt and Pepper');
subplot(2,2,1); imshow(img_sp_fmedzer);
subplot(2,2,2); imshow(img_sp_fmedsym);
subplot(2,2,3); imhist(img_sp_fmedzer, 50);
subplot(2,2,4); imhist(img_sp_fmedsym, 50);

% Filtre img4_s con un filtro de mediana 5x5 con la estrategia zero
% padding y mirror padding en guarda el resultado en img_s_fmedzer y img_s_fmedsym
img_s_fmedzer = medfilt2(img4_s, [5, 5], 'zeros');
img_s_fmedsym = medfilt2(img4_s, [5, 5], 'symmetric');

% Represente en una figura 1x3 img4, img4_s y img_s_fmedzer
figure; 
title('speckle');
subplot(1,3,1); imshow(img4);
subplot(1,3,2); imshow(img4_sp);
subplot(1,3,3); imshow(img_sp_fmedzer);

% Represente en una figura 2x2 img_s_fmedzer, img_s_fmedsym y sus histogramas
figure; 
title('speckle');
subplot(2,2,1); imshow(img_s_fmedzer);
subplot(2,2,2); imshow(img_s_fmedsym);
subplot(2,2,3); imhist(img_s_fmedzer, 50);
subplot(2,2,4); imhist(img_s_fmedsym, 50);

%% Extracción de contornos:
% Carga la imagen 'coins.png'
img2 = imread('coins.png');
